package dev.pee.zopara;

import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.Viewport;

import java.util.Random;

public class Particle extends Circle {
	private Vector2 direction;
	private final Vector2 acceleration = new Vector2(0, -9.81f);
	private double drag = 0.1;
	private final double elasticity;
	private double ttl = 10;

	public Particle(Vector2 position, Vector2 direction, float radius) {
		this.x = position.x;
		this.y = position.y;
		this.direction = direction;
		this.radius = radius;
		Random rnd = new Random();
		elasticity = 0.45 + rnd.nextFloat() * 0.25; // random number between 0.45 and 0.65
	}

	public void Update(float dt, Stage stage) {
		CheckBounce(stage);
		this.direction.mulAdd(this.acceleration, dt * PeeMain.speedMultiplier);
		this.direction.x -= this.direction.x * this.drag * dt;
		this.x += direction.x * dt * PeeMain.speedMultiplier;
		this.y += direction.y * dt * PeeMain.speedMultiplier;
		if (Math.abs(this.direction.y) < 0.05)
			this.drag = 0.5;
	}

	public void CheckBounce(Stage s) {
		if (this.x < 0 && this.direction.x < 0 || this.x > s.getWidth() && this.direction.x > 0) {
			this.direction.x *= -elasticity;
		}
		if (this.y < 0 && this.direction.y < 0 || this.y > s.getHeight() && this.direction.y > 0) {
			this.direction.y *= -elasticity;
		}
	}

	public boolean CheckRemove() {
		return this.y <= 1 && this.direction.len2() < 0.8;
	}
}
