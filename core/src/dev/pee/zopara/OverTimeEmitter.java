package dev.pee.zopara;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;

import java.util.Random;

public class OverTimeEmitter extends Emitter {
	public Vector2 direction;
	public int delta;
	private double deltaTime = 0;
	private double tick = 0.05;

	public OverTimeEmitter(Vector2 position, Vector2 position2, float radius) {
		this.position = position;
		direction = position2.sub(position).nor();
		this.radius = radius;
		this.color = Color.CORAL;
		particleCount = Integer.parseInt(Ui.particleNumberField.getText());
		ttl = particleCount;
		delta = (int) Ui.particleDeltaSlider.getValue();
		particleSize = Ui.particleSizeSlider.getValue();
		PeeMain.emitters.add(this);
	}

	public void Create() {

	}

	public void EmitNext(float dt) {
		deltaTime += dt;
		if (deltaTime < tick) return;
		double ticksInDeltaTime = deltaTime / tick;
		int particlesToSpawn = (int) ticksInDeltaTime;
		deltaTime -= ticksInDeltaTime * tick;
		for (int i = 0; i < particlesToSpawn; i++) {
			Random rnd = new Random();
			PeeMain.particles.add(RandomizeParticle(rnd));
		}
		particleCount -= particlesToSpawn;
		ttl -= particlesToSpawn;
	}

	private Particle RandomizeParticle(Random rnd) {
		int deltaDeg = rnd.nextInt(delta) * (rnd.nextBoolean() ? 1 : -1);
		Vector2 pDirection = new Vector2(direction.cpy().rotateDeg(deltaDeg));
		// replace 10 with user input for speed
		pDirection.scl(rnd.nextFloat() * 10 * PeeMain.speedMultiplier);
		return new Particle(position, pDirection, (float) particleSize);
	}
}
