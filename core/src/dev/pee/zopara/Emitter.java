package dev.pee.zopara;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;

public class Emitter {
	public Vector2 position;
	public float radius;
	public int particleCount;
	public double particleSize;
	public double ttl;
	public Color color;
}
