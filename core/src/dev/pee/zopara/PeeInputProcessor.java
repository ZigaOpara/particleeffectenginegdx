package dev.pee.zopara;

import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.math.Vector2;

public class PeeInputProcessor implements InputProcessor {
	private Vector2 lastTouch = new Vector2();

	@Override
	public boolean keyDown(int keycode) {
		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		String emitterType = Ui.emitterTypeSelect.getSelected().toString();
		switch (emitterType) {
			case "Burst":
				BurstEmitter emitter = new BurstEmitter(new Vector2(screenX, PeeMain.stage.getHeight() - screenY), 6, 10);
				emitter.Create();
				return true;
			case "Over-time":
				lastTouch.set(screenX, PeeMain.stage.getHeight() - screenY);
				return true;
		}
		return false;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		String emitterType = Ui.emitterTypeSelect.getSelected().toString();
		if (emitterType.equals("Over-time") && (screenX != lastTouch.x && screenY != lastTouch.y)) {
			OverTimeEmitter emitter = new OverTimeEmitter(lastTouch, new Vector2(screenX, PeeMain.stage.getHeight() - screenY), 6);
			emitter.Create();
			return true;
		}
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		return false;
	}

	@Override
	public boolean scrolled(float amountX, float amountY) {
		return false;
	}
}
